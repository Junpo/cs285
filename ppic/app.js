var express = require('express')
var path = require('path')
var app = express()
app.use('/static',express.static(path.join(_dirname,'public')))

app.get('/', function(req, res){
  res.sendFile(path.join(_dirname, 'flight.html'))
})
app.get('/Template-Display-Flights.html',function (req,res){
  res.sendFile(path.join(_dirname,'Template-Display-Flights.html'))
})

app.listen(3000,function(){
  console.log('Server started at port 3000')
})
