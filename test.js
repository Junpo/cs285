var http = require('http')
var port = 3000
var serverUrl = 'localhost'
var fs = require('fs')
var express = require('express')
var app = express()
var path = require('path');

// var bodyParser = require('body-parser')
// app.use(bodyParser())
var trip = [{
  price : 5000,
  airline : "Kan Airline",
  takeoff : "20.00",
  landing : "01.00",
  duration : '1hr 15m',
},
{
  price : 500,
  airline : "Flying Pig Airline",
  takeoff : "20.00",
  landing : "01.00",
  duration : '1hr 15m',
},
{
  price : 1000,
  airline : "North Korea Airline",
  takeoff : "20.00",
  landing : "01.00",
  duration : '1hr 20m',
},
{
  price : 5,
  airline : "South Africa Airline",
  takeoff : "15.00",
  landing : "22.00",
  duration : '5hr 20m',
}
]
app.get('/getjson',function(req,res){
    res.send(trip);
})
app.use('/static',express.static(path.join(__dirname + '/public')))
app.get('/',function(req,res){
    res.redirect('/searchFlights');
})
app.get('/searchFlights',function(req,res){
  res.sendFile(path.join(__dirname+'/searchFlights.html'));
})
app.get('/displayFlights',function(req,res){
  res.sendFile(path.join(__dirname+'/displayFlights.html'));
})
app.listen(port,function(){
  console.log('connected at port : ' + port);
})
